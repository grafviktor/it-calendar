import { combineReducers } from 'redux';
import absenceInstrument from './absence_instrument';
import usersList from './server_users_list';
import serverAbsenceList from './server_absence_list';
import error from './error';

export default combineReducers({error, absenceInstrument, usersList, serverAbsenceList});
import { ABSENCE_INSTRUMENT_CHANGED } from '../constants';

export default (absenceInstrumentState = null, action) => {
  const { type, payload } = action;

  switch(type) {
    case ABSENCE_INSTRUMENT_CHANGED:
      return payload.absenceInstrument;

    default:
      return absenceInstrumentState;
  }
}
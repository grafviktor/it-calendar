import axios from 'axios';

import {
  ABSENCE_INSTRUMENT_CHANGED,
  USER_LIST_CHANGED,
  SERVER_ABSENCE_LIST_UPDATED,
  SERVER_STORE_ABSENCE,
} from '../constants';

export function absenceInstrumentChange(absenceInstrument) {
  return {
    type: ABSENCE_INSTRUMENT_CHANGED,
    payload: { absenceInstrument },
  }
}

export function getUsersList() {
  return {
    type: USER_LIST_CHANGED,
    payload: axios.get('/api/users'),
  };
}

export function getExistingAbsenceList() {
  return {
    type: SERVER_ABSENCE_LIST_UPDATED,
    payload: axios.get('/api/absence'),
  };
}

export function storeAbsenceOnServer(id, date, type) {
  return {
    type: SERVER_STORE_ABSENCE,
    payload: axios.post('/api/add_absence', {
      id,
      time: date.getTime(),
      type,
    }),
  };
}
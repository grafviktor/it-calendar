const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const users = require('./users');

app.use(bodyParser.json());

const PORT = 3001;

const absenceList = [
  {
    id: 0,
    absenceList: {},
  },

  {
    id: 1,
    absenceList: {},
  },

  {
    id: 2,
    absenceList: {
      [new Date(2018, 0, 1).getTime()]: 1,
      [new Date(2018, 0, 2).getTime()]: 2,
      [new Date(2018, 1, 5).getTime()]: 2,
      [new Date(2018, 1, 8).getTime()]: 3,
      [new Date(2018, 1, 11).getTime()]: 1,
      [new Date(2018, 1, 12).getTime()]: 1,
      [new Date(2018, 1, 13).getTime()]: 1,
      [new Date(2018, 1, 14).getTime()]: 1,
    },
  },

  {
    id: 3,
    absenceList: {},
  },

  {
    id: 4,
    absenceList: {},
  },

  {
    id: 5,
    absenceList: {},
  },

  {
    id: 6,
    absenceList: {},
  },
];

//   {
//     id: 4,
//     absenceList: [
//       { date: new Date(2018, 1, 3).getTime(), type: 1 },
//       { date: new Date(2018, 2, 4).getTime(), type: 2 },
//       { date: new Date(2018, 2, 5).getTime(), type: 2 },
//       { date: new Date(2018, 2, 8).getTime(), type: 3 },
//       { date: new Date(2018, 4, 11).getTime(), type: 2 },
//       { date: new Date(2018, 4, 12).getTime(), type: 2 },
//       { date: new Date(2018, 4, 13).getTime(), type: 2 },
//       { date: new Date(2018, 2, 14).getTime(), type: 2 },
//     ]
//   },

//   {
//     id: 6,
//     absenceList: [
//       { date: new Date(2018, 3, 13).getTime(), type: 1 },
//       { date: new Date(2018, 3, 14).getTime(), type: 2 },
//       { date: new Date(2018, 3, 15).getTime(), type: 2 },
//       { date: new Date(2018, 4, 18).getTime(), type: 3 },
//       { date: new Date(2018, 4, 21).getTime(), type: 4 },
//       { date: new Date(2018, 4, 22).getTime(), type: 4 },
//       { date: new Date(2018, 5, 23).getTime(), type: 4 },
//       { date: new Date(2018, 5, 24).getTime(), type: 4 },
//     ]
//   },
// ]

function storeInDatabase(id, time, type) {
  const userData = absenceList.find(userData => id === userData.id);
  if (type > 0) {
    userData.absenceList[time] = type;
  } else {
    delete userData.absenceList[time];
  }

  console.log(absenceList);
}

app.get('/api/absence', (req, res) => res.json(absenceList));

app.get('/api/users', (req, res) => {
  users(null, (error, results, fields) => {
    if (error) {
      res.statusCode = 500;
      res.json(error);
    } else {
      res.json(results);
    }
  });
});

app.post('/api/add_absence', (req, res) => {
  const { id, time, type } = req.body;
  res.end();
  storeInDatabase(id, time, type);
});

app.listen(PORT, () => console.log(`Backend is listening on port ${PORT}!`))
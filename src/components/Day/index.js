import React from 'react';

import './index.css';

class Day extends React.Component {
  constructor(props) {
    super(props);
    this.date = this.props.date;
  }

  getClassName = () => {
    return this.date.getDay() % 6 ? 'day' : 'day  day--wekend';
  }

  render() {
    console.log(this.constructor.name, 'render');
    return <div className={this.getClassName()} >{this.date.getDate()}</div>;
  }
}

export default Day;

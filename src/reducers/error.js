export default (errorState = null, action) => {
  const { type, payload, error } = action;

  if (action.error) {
    const error = payload.response.data;
    console.error(JSON.stringify(error));
    return error;
  }

  return errorState;
};
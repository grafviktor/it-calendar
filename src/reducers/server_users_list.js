import { USER_LIST_CHANGED } from '../constants';

export default (usersState = null, action) => {
  const { type, payload, error } = action;

  if (error) return usersState;

  switch(type) {
    case USER_LIST_CHANGED:
      return payload.data;

    default:
      return usersState;
  }
}
import React from 'react';

import './index.css';

const weekdays = new Array(7);
weekdays[0] = 'Sun';
weekdays[1] = 'Mon';
weekdays[2] = 'Tue';
weekdays[3] = 'Wed';
weekdays[4] = 'Thu';
weekdays[5] = 'Fri';
weekdays[6] = 'Sat';

function WeekDay({day}) {
  return (
    <div className="weekday">
      {weekdays[day]}
    </div>
  )
}

export default WeekDay;
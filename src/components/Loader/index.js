import React from 'react';
import './index.css';

export default function Loader() {
  return (
    <div className="scene">
      <div className="message">Loading data...</div>
      <div className="spinner"></div>
    </div>
  );
}
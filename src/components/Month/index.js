import React from 'react';
import { connect } from 'react-redux';
import { PropTypes } from 'prop-types';

import User from '../User';
import Day from '../Day';
import WeekDay from '../WeekDay';
import AbsenceToggler from '../AbsenceToggler';

import './index.css';

class Month extends React.PureComponent {

  constructor(props) {
    super(props);
    this.month = props.month;
    this.year = props.year;
    this.daysTotal = new Date(this.year, this.month+1, 0).getDate();
    this.state = {
      users: [],
    };

    this.users = this.props.users;
  }

  getMonthName = () => {
    return new Date(this.year, this.month).toLocaleString('en-us', {month: 'long'});
  }

  getWeekDaysRow() {
    const headerCells = Array(this.daysTotal).fill().map((_, index) => {
      const date = new Date(this.year, this.month, index+1);
      return (
        <th key={index} className="month__cell" >
          <WeekDay day={date.getDay()} />
        </th>
      );
    });

    const weekDaysRow = (
      <tr>
        <th>{/* nothing */}</th>
        {headerCells}
      </tr>
    );

    return weekDaysRow;
  }

  getDatesRow = () => {
    const headerCells = Array(this.daysTotal).fill().map((_, index) => {
      const date = new Date(this.year, this.month, index+1);
      return (
        <th key={index} className="month__cell" >
          <Day date={date} />
        </th>
      );
    });

    const dateRow = (
      <tr>
        <th>Employee Name</th>
        {headerCells}
      </tr>
    );

    return dateRow;
  }

  confrontWithServerData(userId, date) {
    const { serverAbsenceList } = this.props;
    const userData = serverAbsenceList.find(user => {
      return user.id === userId
    });
    return (userData && userData.absenceList[date.getTime()]) || 0;
  }

  getTogglers = (id) => {
    return Array(this.daysTotal).fill().map((_, index) => {
      const date = new Date(this.year, this.month, index+1);
      const storedAbsenceInstrument = this.confrontWithServerData(id, date);
      return (
        <td key={index} className="month__cell" >
          <AbsenceToggler id={id} date={date} storedAbsenceInstrument={storedAbsenceInstrument} />
        </td>
      );
    });
  }

  getUserRows() {
    return this.users.map((user) => {
      return (
        <tr className="month__row" key={user.id}>
          <td className="month__cell--username"><User user={user.username} /></td>
          {this.getTogglers(user.id)}
        </tr>
      )});
  }

  render() {
    console.log(this.constructor.name, 'render');

    if(!this.props.serverAbsenceList) return null;

    const month = this.getMonthName();
    const weekDaysRow = this.getWeekDaysRow();
    const datesRow = this.getDatesRow();
    const userRows = this.getUserRows();

    return (
      <table className="month">
        <caption className="month__name">{month}</caption>
        <thead>
          {weekDaysRow}
          {datesRow}
        </thead>
        <tbody>
          {userRows}
        </tbody>
      </table>
    );
  }
}

const mapStateToProps = ({serverAbsenceList}) => ({serverAbsenceList});
export default connect(mapStateToProps)(Month);

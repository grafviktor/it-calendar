import React from 'react';

import Month from '../Month';

import './index.css';

function getMonths(year, users) {
  return Array(12).fill().map((_, index) => <Month key={index} year={year} month={index} users={users} />);
}

function Calendar({ year, users }) {
  const months = getMonths(year, users);
  return (
    <div className="calendar">
      {months}
    </div>
  );
}

export default Calendar;

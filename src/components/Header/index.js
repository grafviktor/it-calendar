import React from 'react';

import './index.css';
import AbsenceInstrument from '../AbsenceInstrument';

class Header extends React.Component {
  render() {
    console.log(this.constructor.name, 'render');
    return (
      <header className={`${this.props.className} header`}>
        <h2 className="header__title">EMEA Employee Absence Schedule</h2>
        <h2 className="header__title">{this.props.year}</h2>
        <AbsenceInstrument />
      </header>
    );
  }
}

export default Header;
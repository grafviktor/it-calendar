import React, { Fragment } from 'react';
import { connect } from 'react-redux';

import './index.css';

class Error extends React.Component {
  state = {
    error: this.props.error,
  };

  onClickHandler = () => {
    this.setState({ error: null });
  }

  componentWillReceiveProps(nextProps) {
    this.setState({ error: nextProps.error });
  }

  getBody = (error) => {
    const errorDetails = Object.keys(error).map(key => {
      // debugger;
      return (
        <Fragment key={key}>
          <dt className="error__name">{key}</dt>
          <dd className="error__description">{error[key]}</dd>
        </Fragment>
      );
    });
    return (
      <div className="error">
        <h3 className="error__title">Error</h3>
        <div className="error__close-button" onClick={this.onClickHandler} />
        <dl className="error__details">
          {errorDetails}
        </dl>
      </div>
    );
  }

  render() {
    return this.state.error ? this.getBody(this.state.error) : null;
  }
}

const mapStateToProps = (({error}) => ({error}));
export default connect(mapStateToProps)(Error);
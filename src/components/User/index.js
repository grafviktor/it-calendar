import React from 'react';

import './index.css';

function User({user}) {
  return <div className="user">{user}</div>;
}

export default User;

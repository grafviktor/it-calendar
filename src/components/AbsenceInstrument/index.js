import React from 'react';
import { connect } from 'react-redux';

import { ABSENCE_INSTRUMENT_STATES } from '../../constants';
import { absenceInstrumentChange } from '../../actions';

import './index.css';

class AbsenceInstrument extends React.Component {

  onAbsenceInstrumentChange = (event) => {
    this.props.absenceInstrumentChange(Number(event.target.value));
  }

  render() {
    console.log(this.constructor.name, 'render');
    return (
      <div className="absence">
        <h3 className="absence__heading">Absence Type Key</h3>
        <div className="absence__buttons">
          <input className="absence__type" id="vacation" onChange={this.onAbsenceInstrumentChange} type="radio" name="absence_state" value={ABSENCE_INSTRUMENT_STATES.vacation} />
          <label className="absence__button  absence__button--vacation" htmlFor="vacation">V</label>
          <span className="absence__name">Vacation</span>

          <input className="absence__type" id="personal" onChange={this.onAbsenceInstrumentChange} type="radio" name="absence_state" value={ABSENCE_INSTRUMENT_STATES.personal} />
          <label className="absence__button  absence__button--personal" htmlFor="personal">A</label>
          <span className="absence__name">Personal</span>

          <input className="absence__type" id="sick" onChange={this.onAbsenceInstrumentChange} type="radio" name="absence_state" value={ABSENCE_INSTRUMENT_STATES.sickness} />
          <label className="absence__button  absence__button--sickness" htmlFor="sick">S</label>
          <span className="absence__name">Sickness</span>

          <input className="absence__type" id="planned" onChange={this.onAbsenceInstrumentChange} type="radio" name="absence_state" value={ABSENCE_INSTRUMENT_STATES.planned} />
          <label className="absence__button  absence__button--planned" htmlFor="planned">P</label>
          <span className="absence__name">Planned</span>

          <input className="absence__type" id="bank_holiday" onChange={this.onAbsenceInstrumentChange} type="radio" name="absence_state" value={ABSENCE_INSTRUMENT_STATES.holiday} />
          <label className="absence__button  absence__button--bank_holiday" htmlFor="bank_holiday">B</label>
          <span className="absence__name">Bank/Public Holiday</span>
        </div>
      </div>
    );
  }
}

const mapDispatchToProps = { absenceInstrumentChange };
export default connect(null, mapDispatchToProps)(AbsenceInstrument);

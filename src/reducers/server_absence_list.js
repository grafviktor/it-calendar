import { SERVER_ABSENCE_LIST_UPDATED } from '../constants';

export default (absenceState = null, action) => {
  const { type, payload, error } = action;

  if (error) return absenceState;

  switch(type) {
    case SERVER_ABSENCE_LIST_UPDATED:
      return payload.data;

    default:
      return absenceState;
  }
}
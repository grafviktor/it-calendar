import React, { Component } from 'react';
import { connect } from 'react-redux';

import './index.css';
import Header from './components/Header';
import Calendar from './components/Calendar';
import Loader from './components/Loader';
import Error from './components/Error';

import { getUsersList, getExistingAbsenceList } from './actions';

class App extends Component {
  state = {
    year: new Date().getFullYear(),
  }

  componentDidMount() {
    this.props.getUsersList();
    this.props.getExistingAbsenceList();
  }

  render() {
    console.log(this.constructor.name, 'render');
    const { usersList } = this.props;
    const main = usersList ? <Calendar year={this.state.year} users={usersList} /> : <Loader />
    return (
      <div className="app">
        <Error />
        <Header className="app__header" year={this.state.year} />
        <main className="app__main">
          {main}
        </main>
      </div>
    );
  }
}

const mapStateToProps = ({ usersList }) => ({usersList});
const mapDispatchToProps = { getUsersList, getExistingAbsenceList };
export default connect(mapStateToProps, mapDispatchToProps)(App);

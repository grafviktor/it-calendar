import React from 'react';
import { connect } from 'react-redux';
import { PropTypes } from 'prop-types';

import { storeAbsenceOnServer } from '../../actions';
import { ABSENCE_INSTRUMENT_STATES } from '../../constants';

import './index.css';

class VacationToggler extends React.Component {
  constructor(props) {
    super(props);
    this.date = props.date;
    this.id = props.id;
    this.state = {
      absenceInstrument: props.storedAbsenceInstrument || 0,
      style: null,
    }
  }

  shouldComponentUpdate(nextProps) {
    if(nextProps.absenceInstrument !== this.props.absenceInstrument)
      return false;

    return true;
  }

  onClickHandler = (event) => {
    if(!this.props.absenceInstrument) return;

    let absenceInstrument;
    // set state to default value if absenceInstrument has been changed
    if(this.state.absenceInstrument !== 0 && this.state.absenceInstrument !== this.props.absenceInstrument) {
      absenceInstrument = 0
    } else {
      absenceInstrument = this.state.absenceInstrument ^ this.props.absenceInstrument;
    }
    this.setState({ absenceInstrument });
    this.props.storeAbsenceOnServer(this.props.id, this.props.date, absenceInstrument);
  }

  getClassName = () => {
    let classNames = 'vacation-toggler';
    if (this.state.absenceInstrument) {
      let absenceInstrumentState = Object.keys(ABSENCE_INSTRUMENT_STATES).find(key => ABSENCE_INSTRUMENT_STATES[key] === this.state.absenceInstrument);
      classNames += `  vacation-toggler--${absenceInstrumentState}`;
    }
    return classNames;
  }

  onMouseEnterHandler = (event) => {
    event.preventDefault();
    const style = this.props.absenceInstrument ? { cursor: 'pointer' } : null;
    this.setState({ style });
  }

  render() {
    console.log(this.constructor.name, 'render', Date.now());
    return <div className={this.getClassName()}
      onClick={this.onClickHandler}
      onMouseEnter={this.onMouseEnterHandler}
      style={this.state.style} />;
  }
}

const mapStateToProps = ({absenceInstrument}) => ({ absenceInstrument });
const mapDispatchToProps = { storeAbsenceOnServer };
export default connect(mapStateToProps, mapDispatchToProps)(VacationToggler);

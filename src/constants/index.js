export const USER_LIST_CHANGED = 'USER_LIST_CHANGED';

export const ABSENCE_INSTRUMENT_CHANGED = 'ABSENCE_INSTRUMENT_CHANGED';

export const SERVER_ABSENCE_LIST_UPDATED = 'SERVER_ABSENCE_LIST_UPDATED';

export const SERVER_STORE_ABSENCE = 'SERVER_STORE_ABSENCE';

export const ABSENCE_INSTRUMENT_STATES = {
  'vacation': 1,
  'personal': 2,
  'sickness': 3,
  'planned':  4,
  'holiday':  5,
}
